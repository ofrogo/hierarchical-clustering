import numpy as np
import pandas as pd
from scipy.cluster.vq import kmeans
from scipy.spatial.distance import cdist
from sklearn.preprocessing import normalize

rnd_df = pd.read_csv('random3k.tsv', sep='\t')


# right_words = set(pd.read_csv('prettyDataSet.tsv', sep='\t').to_numpy().flatten())
# a = rnd_df[[x for x in right_words if x in rnd_df]].to_numpy()
# pca = PCA(n_components=2)
#
# file_pca = pca.fit_transform(normalize(a))
# plt.figure(figsize=[20, 20])
# plt.plot(file_pca[:, 0], file_pca[:, 1], '.g')
# plt.savefig('pca.png')
# plt.close()


def kmeans_export(centroids, labels):
    res = [[] for _ in range(len(centroids))]
    d = cdist(np.array(data), centroids, 'euclidean')
    for i, l in enumerate(d):
        res[l.tolist().index(l.min())].append(labels[i])
    return res


rnd_df.drop([x for x in list(rnd_df)[1:] if sum(rnd_df[x]) < 0.1], axis='columns', inplace=True)
data = normalize(rnd_df.loc[:, 'Алексий':].to_numpy(dtype=float))
centroids, distortions = kmeans(data, 33)
K_res = kmeans_export(centroids, rnd_df.to_numpy()[:, 0])

with open('result.txt', 'w') as file:
    for line in K_res:
        for i in line:
            file.write(f'{i} ')
        file.write('\n')
        for _ in range(100):
            file.write('-')
        file.write('\n')
