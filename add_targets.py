import pandas as pd

rnd_df = pd.read_csv('random3k.tsv', sep='\t')
right_words = pd.read_csv('prettyDataSet.tsv', sep='\t')
for index, row in right_words.iterrows():
    rnd_df.loc[len(rnd_df)] = [0. for _ in range(rnd_df.shape[1])]
    rnd_df.loc[len(rnd_df) - 1, 'features'] = row['Name']
    for word in row.loc['0':'19']:
        if word in rnd_df:
            rnd_df.loc[len(rnd_df) - 1, word] = 0.9
rnd_df.to_csv('random3k_tar.tsv', sep='\t')