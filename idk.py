from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.preprocessing import normalize

rnd_df = pd.read_csv('random3k.tsv', sep='\t')
rnd_df.drop([x for x in list(rnd_df)[1:] if sum(rnd_df[x]) < 0.1], axis='columns', inplace=True)
data = normalize(rnd_df.loc[:, 'Алексий':].to_numpy(dtype=float))

inertia = []
for k in range(1, 33):
    kmeans = KMeans(n_clusters=k, random_state=1).fit(data)
    inertia.append(np.sqrt(kmeans.inertia_))

plt.plot(range(1, 33), inertia, marker='s')
plt.xlabel('$k$')
plt.ylabel('$J(C_k)$')
plt.savefig('inertia.png')
