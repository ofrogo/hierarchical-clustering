import pandas as pd
from sklearn import preprocessing

df = pd.read_csv('test.tsv', sep='\t')

np_df = df.to_numpy()
np_df[:, 1:] = preprocessing.normalize(np_df[:, 1:])

result_df = pd.DataFrame(np_df, columns=df.columns)
result_df.to_csv('norm_test.tsv', '\t', index=False)
