from sklearn.decomposition import PCA
import numpy as np
import pandas as pd


np_df = pd.read_csv('norm_test.tsv', sep='\t').to_numpy()[:, 1:]

pca = PCA(n_components=4000)
principalComponents = pca.fit_transform(np_df)

print(principalComponents.shape)
print(sum(pca.explained_variance_))
print(sum(pca.explained_variance_ratio_))
print(pca.explained_variance_ratio_.cumsum())