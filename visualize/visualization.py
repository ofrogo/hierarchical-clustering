import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import pandas as pd
from sklearn.preprocessing import normalize


file = pd.read_csv('//filename//', sep='\t')
file_np = file.to_numpy()[:, 1:]
file_np_norm = normalize(file_np)

tsne_2 = TSNE()
pca_2 = PCA(n_components=2)

file_pca_2 = pca_2.fit_transform(file_np_norm)
file_tsne_2 = tsne_2.fit_transform(file_np_norm)

plt.figure(figsize=[20, 20])
plt.plot(file_pca_2[:, 0], file_pca_2[:, 1], '.g')
plt.savefig('//filename.png//')
plt.close()

plt.figure(figsize=[20, 20])
plt.plot(file_tsne_2[:, 0], file_tsne_2[:, 1], '.b')
plt.savefig('//filename.png//')
plt.close()


